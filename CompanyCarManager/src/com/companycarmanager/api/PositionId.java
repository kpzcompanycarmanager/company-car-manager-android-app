package com.companycarmanager.api;

public class PositionId {
	private int positionId;
	
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
}
