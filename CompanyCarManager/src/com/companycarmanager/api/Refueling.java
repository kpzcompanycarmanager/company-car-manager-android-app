package com.companycarmanager.api;

public class Refueling {
	
	private String error;
	
	private String userName;
	private String password;
	
	private int positionId;
	private double amount;
	private double costPerLitre;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getCostPerLitre() {
		return costPerLitre;
	}
	public void setCostPerLitre(double costPerLitre) {
		this.costPerLitre = costPerLitre;
	}

}
