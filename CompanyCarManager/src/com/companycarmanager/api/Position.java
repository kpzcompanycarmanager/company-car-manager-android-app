package com.companycarmanager.api;


public class Position {

		private String error;
		
		private String userName;
		private String password;
		
		private double longitude;
		private double latitude;
		private int carId;
		private long date;
		
		public String getError() {
			return error;
		}
		public void setError(String error) {
			this.error = error;
		}
		public double getLongitude() {
			return longitude;
		}
		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}
		public double getLatitude() {
			return latitude;
		}
		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public int getCarId() {
			return carId;
		}
		public void setCarId(int carId) {
			this.carId = carId;
		}
		public long getDate() {
			return date;
		}
		public void setDate(long date) {
			this.date = date;
		}
		
 

}
