package com.companycarmanager.api;

public class UserRefueling {

	private int positionId;
	private double amount;
	private double costPerLitre;
	
	public  UserRefueling (int posId, double amount, double cPL){
		this.positionId = posId;
		this.amount = amount;
		this.costPerLitre = cPL;
	}
}
