package com.companycarmanager.api;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;

public interface RestAPI {

	

	@POST("/api/cars/select")
	public void selectCar(@Body UserDetails body, Callback<?> callback);
	
	@POST("/api/cars/available")
	public void getCars(@Body UserDetails body, 
				Callback<List<CarDetails>> callback);
	

	@POST("/api/positions/add")
	public void updatePosition(@Body UserDetails body, Callback<PositionId> callback);
	
	
	
	@POST("/api/refuelings/add")
	public void updateRefueling(@Body UserDetails body, Callback<?> callback);
}
