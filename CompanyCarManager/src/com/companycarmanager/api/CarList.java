package com.companycarmanager.api;

import java.util.ArrayList;
import java.util.List;

public class CarList {

		public List<CarDetails> CarList = new ArrayList<CarDetails>();
		
		public List<CarDetails> getCarList() {
			return CarList;
		}
		public void setCarList(List<CarDetails> carList) {
			CarList = carList;
		}
 
}
