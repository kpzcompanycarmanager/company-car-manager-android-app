package com.companycarmanager.api;

public class UserDetails {
	private String userName; // ????
	private String password;
	private int carId;
	
	private UserRefueling refueling;
	private UserPosition position;
	
	public UserDetails(String userName, String password){
		this.userName = userName;
		this.password = password;
	}
	
	public UserDetails(String userName, String password, int carId){
		this.userName = userName;
		this.password = password;
		this.carId = carId;
	}

	public UserRefueling getRefueling() {
		return refueling;
	}

	public void setRefueling(UserRefueling refueling) {
		this.refueling = refueling;
	}

	public void setLocation(UserPosition location) {
		this.position = location;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}
}
