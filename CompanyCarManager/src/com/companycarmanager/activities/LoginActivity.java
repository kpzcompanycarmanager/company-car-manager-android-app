package com.companycarmanager.activities;

import java.util.ArrayList;
import java.util.List;



import com.companycarmanager.api.CarDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends Activity implements Address{

	private Button btnLogin;
	private EditText inputLogin;
	private EditText inputPassword;
	
	
	private List<CarDetails> carList = new ArrayList<CarDetails>();


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		inputLogin = (EditText) findViewById(R.id.login);
		inputPassword = (EditText) findViewById(R.id.password);
		btnLogin = (Button) findViewById(R.id.btnLogin);

		btnLogin.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				String login = inputLogin.getText().toString();
				String password = inputPassword.getText().toString();

				// Check for empty data in the form
				if (login.trim().length() > 0 && password.trim().length() > 0) { // && isOnline()
					// login user
					Login(login, password);


				} else if (login.trim().length() == 0 || password.trim().length() == 0){
					// Prompt user to enter credentials
					Toast.makeText(LoginActivity.this,  //getApplicationContext()
							"Please enter the credentials!", Toast.LENGTH_LONG)
							.show();

				} else if (!isOnline()) {
					Toast.makeText(LoginActivity.this, 
							"Network isn't available", 
							Toast.LENGTH_LONG)
							.show();
				}
			}

		});
	}

	synchronized private void Login(String login, String password) {

		Intent intent = new Intent(LoginActivity.this, CarListActivity.class);
		intent.putExtra("userName", login);		
		intent.putExtra("password", password);
		inputLogin.setText("");
		inputPassword.setText("");
		startActivity(intent);
		
		
//		//intent.putExtra(json, jsonCarList);
//		//updateDisplay();
//
//
//				login = "k1";
//				password = "haslokuriera1";


	}
	//checking if there is internet access
	protected boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		} else {
			return false;
		}
	}
	
//	protected void updateDisplay() {
//
//		CarAdapter adapter = new CarAdapter(this, R.layout.item_car, carList);
//		setListAdapter(adapter);
//	}


}
