package com.companycarmanager.activities;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

import com.companycarmanager.api.RestAPI;
import com.companycarmanager.api.UserDetails;
import com.companycarmanager.api.UserRefueling;
import com.squareup.okhttp.OkHttpClient;

public class Logout implements Address {

	String userName;
	String password;
	int carId = -1;
	
	Logout(String userName, String password){
		this.userName = userName;
		this.password = password;
	}
	
	public void logout() {

		//retrofit tworzenie polecenia
		RestAdapter adapter = new RestAdapter.Builder()
		.setClient(new OkClient(new OkHttpClient()))
		.setEndpoint(ENDPOINT)
		.setLogLevel(RestAdapter.LogLevel.FULL)
		.build();

		RestAPI api = adapter.create(RestAPI.class);
		

		
		api.selectCar(new UserDetails(userName, password, carId), new Callback<Object>() { 
			@Override
			public void failure(final RetrofitError error) {
				android.util.Log.i("example", "Error, body: " + error.toString());
			}

			@Override
			public void success(Object arg0, Response arg1) {
				// Do something with the User object returned
			
			}

		});
	}
}
	
